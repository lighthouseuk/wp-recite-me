const serviceUrl = 'https://api.reciteme.com/asset/js?key=',
	serviceKey = RECITE_ME.apiKey,
	options = {},
	autoLoad = false,
	enableFragment = '#reciteEnable';

if ( RECITE_ME.disabledButtons ) {
	options['Controls'] = {
		'Desktop': {
			'disabledButtons': RECITE_ME.disabledButtons,
		},
	};
}
var loaded=[],frag=!1;window.location.hash===enableFragment&&(frag=!0);function loadScript(c,b){var a=document.createElement('script');a.type='text/javascript';a.readyState?a.onreadystatechange=function(){if("loaded"==a.readyState||"complete"==a.readyState)a.onreadystatechange=null,void 0!=b&&b()}:void 0!=b&&(a.onload=function(){b()});a.src=c;document.getElementsByTagName("head")[0].appendChild(a)}function _rc(c){c+="=";for(var b=document.cookie.split(";"),a=0;a<b.length;a++){for(var d=b[a];" "==d.charAt(0);)d=d.substring(1,d.length);if(0==d.indexOf(c))return d.substring(c.length,d.length)}return null}function loadReciteMe(c){for(var b=serviceUrl+serviceKey,a=0;a<loaded.length;a++)if(loaded[a]==b)return;loaded.push(b);loadScript(serviceUrl+serviceKey,function(){"function"===typeof _reciteLoaded&&_reciteLoaded();"function"==typeof c&&c();Recite.load(options);Recite.Event.subscribe("Recite:load",function(){Recite.enable()})})}"true"==_rc("Recite.Persist")&&loadReciteMe();(autoLoad&&"false"!=_rc("Recite.Persist")||frag)&&loadReciteMe();

function reciteMeDocReady( fn ) {
	if ( 'loading' !== document.readyState ) {
		fn();
		return;
	}
	document.addEventListener( 'DOMContentLoaded', fn );
}

reciteMeDocReady( function() {
	const button = document.querySelector( '.load-reciteme' );
	if ( button ) {
		button.addEventListener( 'click', function( e ) {
			e.preventDefault();
			loadReciteMe();
		} );
	}
} );
