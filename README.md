# Unofficial Recite Me WordPress plugin

[![Latest Stable Version](https://poser.pugx.org/lighthouseuk/wp-recite-me/v)](https://packagist.org/packages/lighthouseuk/wp-recite-me)

A simple plugin to embed [Recite Me](https://reciteme.com/) snippets.

## Installing

Set `RECITE_ME_API_KEY` in your wp-config.php file.

Optionally set `RECITE_ME_DISABLED_BUTTONS` to an array of buttons to disable them.

`define( 'RECITE_ME_DISABLED_BUTTONS', array( 'translate' ) );`

This plugin can be installed using [composer](https://getcomposer.org/)

`composer require lighthouseuk/wp-recite-me`
