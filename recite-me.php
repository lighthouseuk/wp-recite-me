<?php
/**
 * Plugin Name: Recite Me Plugin
 * Description: Unofficial recite me plugin
 * Requires at least: 6.3
 * Version: 1.0.4.1
 * Author: Mike Manger
 * Author URI: https://lighthouseuk.net/
 * Update URI: https://bitbucket.org/lighthouseuk/wp-recite-me/
 * Bitbucket Plugin URI: lighthouseuk/wp-recite-me
 * License: GPLv2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 *
 * @package recite-me
 */

/**
 * Loads init script in site footer.
 */
function recite_me_script(): void {
	wp_enqueue_script( 'recite-me', plugins_url( 'recite-me/js/scripts.js', __DIR__ ), array(), '20250106', array( 'in_footer' => true ) );
	$disabled_buttons = array();
	if ( defined( 'RECITE_ME_DISABLED_BUTTONS' ) ) {
		if ( is_array( RECITE_ME_DISABLED_BUTTONS ) ) {
			$disabled_buttons = RECITE_ME_DISABLED_BUTTONS;
		} elseif ( is_string( RECITE_ME_DISABLED_BUTTONS ) ) {
			$disabled_buttons = array( RECITE_ME_DISABLED_BUTTONS );
		}
	}

	$data = array(
		'apiKey'          => RECITE_ME_API_KEY,
		'disabledButtons' => $disabled_buttons,
	);
	wp_add_inline_script(
		'recite-me',
		'const RECITE_ME = ' . wp_json_encode( $data ),
		'before'
	);
	?>
	<?php
}
if ( defined( 'RECITE_ME_API_KEY' ) ) {
	add_action( 'wp_enqueue_scripts', 'recite_me_script' );
}
